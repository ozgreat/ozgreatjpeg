/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ozgreatJPEG;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

/**
 *
 * @author ozgreat
 */
public class JpegCompress {
    private BufferedImage in;
    private File newJpegFile;
    
    
    JpegCompress(File myImage, File newJpegFile){
        try{
            in = ImageIO.read(myImage);
        }catch(IOException e){
            e.printStackTrace();
        }
        this.newJpegFile = newJpegFile;
        
    }
    
    public void getCompression(float compQuality) throws IOException{
        BufferedImage newBuff = new BufferedImage(in.getWidth(),in.getHeight(),BufferedImage.TYPE_INT_RGB);
        newBuff.createGraphics().drawImage(in, 0, 0, Color.WHITE, null);
        if(newJpegFile.getPath() != null){
            if(!newJpegFile.getPath().endsWith(".jpg")) {
                newJpegFile = new File(newJpegFile.getPath() + ".jpg");
            }else{
                newJpegFile = new File(newJpegFile.getPath());
            }
            Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpg");
            ImageWriter writer = iter.next();
            ImageWriteParam iwp = writer.getDefaultWriteParam();
            iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            iwp.setCompressionQuality(compQuality);
            writer.setOutput(new FileImageOutputStream(newJpegFile));
            writer.write(null, new IIOImage(newBuff,null,null), iwp);
            writer.dispose();
        }
    }
}
