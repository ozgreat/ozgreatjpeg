package ozgreatJPEG;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.collections.*;
import javafx.scene.control.*;
 
public class Main extends Application {
    private File inFile;
    private File outFile;
    private final FileChooser fileChooser = new FileChooser();

    @Override
    public void start(Stage primaryStage) throws Exception {
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("png, jpg, bmp", "*.png", "*.jpg", "*.BMP", "*.bmp"));
        Label status = new Label("Выберете файл для сжатия");
        Button button1 = new Button("Файл");
        Button buttonC = new Button("Compress");
        Button buttonE = new Button("Exit");
        StackPane root = new StackPane();
        ObservableList<String> comprsValue = FXCollections.observableArrayList("0.1","0.2","0.3", "0.4", "0.5", "0.6", "0.7", "0.8", "0.9", "1");
        ComboBox<String> lstCompress = new ComboBox(comprsValue);
        lstCompress.setValue("0.5");
        MenuBar mainMenu = new MenuBar();
        Menu fileMenu = new Menu("File");
        Menu helpMenu = new Menu("Help");
        MenuItem openMI = new MenuItem("Open file");
        MenuItem exitMI = new MenuItem("Exit");
        MenuItem aboutMI = new MenuItem("About");
        fileMenu.getItems().addAll(openMI, exitMI);
        helpMenu.getItems().add(aboutMI);
        mainMenu.getMenus().addAll(fileMenu, helpMenu);
        openMI.setOnAction((ActionEvent event) -> {
            inFile = fileChooser.showOpenDialog(primaryStage);
            if (inFile != null) {
                printLog(status, inFile);
                root.getChildren().addAll(buttonC, lstCompress);
                StackPane.setAlignment(lstCompress, Pos.BOTTOM_CENTER);
                StackPane.setAlignment(buttonC, Pos.BOTTOM_LEFT);
                StackPane.setMargin(buttonC, new Insets(10));
                StackPane.setMargin(lstCompress, new Insets(10));
                   
            }else{
                status.setText("Invalid file!");
            }
        });
 
        buttonC.setOnAction((ActionEvent event) -> {
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPEG", "*.jpg"));
            outFile = fileChooser.showOpenDialog(primaryStage);
            JpegCompress x = new JpegCompress(inFile, outFile);
            try {
                x.getCompression(Float.parseFloat(lstCompress.getValue()));
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            status.setText("Done\nВыберете новый файл");
            root.getChildren().removeAll(buttonC, lstCompress);
        });
        
        exitMI.setOnAction((ActionEvent event) -> {
            Platform.exit();
        });
        
        aboutMI.setOnAction((ActionEvent event) -> {
            Label aboutLabel = new Label("Курсовая работа студента КБИКС 17-1\n"
                    + "ХНУРЭ, факультет КИУ\n"
                    + "\ntg: @ozgreat\ntg канал: @ozcore");
            
            StackPane aboutLayout = new StackPane();
            aboutLayout.getChildren().add(aboutLabel);
            
            Scene secondScene = new Scene(aboutLayout, 300, 200);
            
            //New window (Stage)
            Stage aboutWindow = new Stage();
            aboutWindow.setTitle("ABOUT");
            aboutWindow.setScene(secondScene);
            
            aboutWindow.show();
        });
        
        root.getChildren().addAll(status, mainMenu);
        root.setAlignment(Pos.CENTER);
        StackPane.setAlignment(button1, Pos.BOTTOM_LEFT);
        StackPane.setAlignment(buttonE, Pos.BOTTOM_RIGHT);
        StackPane.setAlignment(mainMenu, Pos.TOP_CENTER);
        
        Scene scene = new Scene(root, 700, 400);
 
        primaryStage.setTitle("ozgreatJPEG");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
 
    private void printLog(Label textArea, File file) {
        if (file == null) {
            return;
        }
        textArea.setText(file.getAbsolutePath() + "\n");
    }
 
 
    public static void main(String[] args) {
        Application.launch(args);
    }
 
}